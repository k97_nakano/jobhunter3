package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.domain.DbAccountDetails;
import com.example.entity.Account;
import com.example.repository.AccountRepository;

@Service
public class RegisterAccountService implements UserDetailsService{
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = accountRepository.findByUsername(username);
 
		if (account == null) {
			throw new UsernameNotFoundException("not found");
		}
		return new DbAccountDetails(account);
	}

}
