package com.example.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dto.UserRequestApplicationForm;
import com.example.entity.Account;
import com.example.entity.ApplicationForm;
import com.example.repository.ApplicationFormRepository;

@Service
public class ApplicationFormService {

	@Autowired
	ApplicationFormRepository applicationFormRepositry;

	public void create(UserRequestApplicationForm userRequestAppForm, Account account) {
		applicationFormRepositry.save(CreateAppForm(userRequestAppForm, account));
	}

	private ApplicationForm CreateAppForm(UserRequestApplicationForm userRequestAppForm, Account account) {

		ApplicationForm appForm = new ApplicationForm();

		if (userRequestAppForm.getHiniti().equals("")) {
			appForm.setHiniti("9999-12-31");
		} else {
			appForm.setHiniti(userRequestAppForm.getHiniti());
		}

		LocalDateTime d = LocalDateTime.now();
		DateTimeFormatter df1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		String s = df1.format(d);
		appForm.setCurrent(s);
		appForm.setStarttime(userRequestAppForm.getStarttime());
		appForm.setEndtime(userRequestAppForm.getEndtime());
		appForm.setBasyo(userRequestAppForm.getBasyo());
		appForm.setNaiyou(userRequestAppForm.getNaiyou());
		appForm.setKigyoumei1(userRequestAppForm.getKigyoumei1());
		appForm.setKigyoumei2(userRequestAppForm.getKigyoumei2());
		appForm.setKigyoumei3(userRequestAppForm.getKigyoumei3());

		if (userRequestAppForm.getKessekibi().equals("")) {
			appForm.setKessekibi("9999-12-31");
		} else {
			appForm.setKessekibi(userRequestAppForm.getKessekibi());
		}

		if (userRequestAppForm.getSoutaibi().equals("")) {
			appForm.setSoutaibi("9999-12-31");
		} else {
			appForm.setSoutaibi(userRequestAppForm.getSoutaibi());
		}

		appForm.setSoutaijikan(userRequestAppForm.getSoutaijikan());

		if (userRequestAppForm.getTikokubi().equals("")) {
			appForm.setTikokubi("9999-12-31");
		} else {
			appForm.setTikokubi(userRequestAppForm.getTikokubi());
		}

		appForm.setTikokujikan(userRequestAppForm.getTikokujikan());
		appForm.setSoutaijikan(userRequestAppForm.getSoutaijikan());
		appForm.setMemo(userRequestAppForm.getMemo());
		appForm.setKomento(userRequestAppForm.getKomento());
		appForm.setAccount(account);
		appForm.setAppstatus("0");
		appForm.setPermissionrefusal("申請中");
		return appForm;
	}

	public List<ApplicationForm> findAll() {
		return applicationFormRepositry.findAll();
	}

	public List<ApplicationForm> findAlluserid(String userName) {
		return applicationFormRepositry.findAlluserid(userName);
	}

	public ApplicationForm getOne(String id) {
		return applicationFormRepositry.getOne(id);
	}

	public void delete(String id) {
		applicationFormRepositry.delete(getOne(id));
	}

	public ApplicationForm save(ApplicationForm appForm, Account account) {
		appForm.setAccount(account);
		return applicationFormRepositry.save(appForm);
	}

	public ApplicationForm update(ApplicationForm appForm, Account account) {
		appForm.setAccount(account);
		return applicationFormRepositry.save(appForm);
	}

	public List<ApplicationForm> findAllstatus() {
		return applicationFormRepositry.findAllstatus();
	}

	public Integer saveKomento(String komento,String id) {
		return applicationFormRepositry.saveKomento(komento,id);
	}


}
