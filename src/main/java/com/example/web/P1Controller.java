package com.example.web;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.domain.DbAccountDetails;
import com.example.dto.UserRequestApplicationForm;
import com.example.entity.ApplicationForm;
import com.example.repository.AccountRepository;
import com.example.service.ApplicationFormService;

@Controller
public class P1Controller {
	@Autowired
	ApplicationFormService applicationFormService;
	
	@Autowired
	AccountRepository accountRepository;

	@GetMapping("/p1create")
	String p1create() {
		return "p1/p1create";
	}

	/*@GetMapping("/p1edit")
	String p1edit() {
		return "p1/p1edit";
	}*/

	/*
	 * @RequestMapping(value = "/p1main", method = RequestMethod.GET) public String
	 * index(Model model) { List<ApplicationForm> Applist =
	 * applicationFormService.findAll(); model.addAttribute("Applist", Applist);
	 * return "p1/p1main"; }
	 */
	
	@RequestMapping(value = "/p1main", method = RequestMethod.GET)
	public String p1main(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		String userName = auth.getName();
		String judgmentstu = accountRepository.judgmentstudent(userName);
		if (judgmentstu.equals("0")) {
			return "redirect:/menu1";
		}else {
		List<ApplicationForm> Applist = applicationFormService.findAlluserid(userName);
		model.addAttribute("Applist", Applist);
		return "p1/p1main";
		}
	}

	@RequestMapping(value = "/p1create", method = RequestMethod.POST)
	public String create(@ModelAttribute UserRequestApplicationForm userRequestAppForm,
			@AuthenticationPrincipal DbAccountDetails dbAccountDetails) {
		applicationFormService.create(userRequestAppForm, dbAccountDetails.getAccount());
		return "redirect:/p1main";
	}

	@GetMapping("/p1/{id}/p1edit")//編集 
	  public String edit(@PathVariable String id,Model model) { 
		  ApplicationForm Appform = applicationFormService.getOne(id);
		  model.addAttribute("Appform",Appform);
	  return "p1/p1edit"; 
	  }
	
	@PostMapping("/p1/{id}")//更新
	public String update(@PathVariable String id,@ModelAttribute ApplicationForm appForm,@AuthenticationPrincipal DbAccountDetails dbAccountDetails) {
		appForm.setId(id);
		LocalDateTime d = LocalDateTime.now();
		DateTimeFormatter df1 = 
				DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		String s = df1.format(d);
		appForm.setCurrent(s);
		appForm.setAppstatus("0");
		appForm.setPermissionrefusal("申請中");
		applicationFormService.save(appForm,dbAccountDetails.getAccount());
		return "redirect:/p1main";
	}

	@PostMapping("/p1/delete") // 削除
	public String delete(String id) {
		applicationFormService.delete(id);
		return "redirect:/p1main";
	}

}
