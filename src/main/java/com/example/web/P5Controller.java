package com.example.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.entity.ReportForm;
import com.example.repository.AccountRepository;
import com.example.service.ReportFormService;

@Controller
public class P5Controller {
	
	@Autowired
	ReportFormService reportFormService;
	
	@Autowired
	AccountRepository accountRepository;

	@GetMapping("/p5fail")
	String p5fail() {
		return "p5/p5fail";
	}

	@GetMapping("/p5success")
	String p5success() {
		return "p5/p5success";
	}

	@GetMapping("/p5result")
	String p5result() {
		return "p5/p5result";
	}
	
	@RequestMapping(value = "/p5main", method = RequestMethod.GET)
	public String p5main(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		String userName = auth.getName();
		String judgmenttea = accountRepository.judgmentteacher(userName);
		if (judgmenttea.equals("0")) {
			return "redirect:/menu1";
		}else {
		List<ReportForm> Replist = reportFormService.findAllstatus();
		model.addAttribute("Replist", Replist);
		return "p5/p5main";
		}//
	}

}
