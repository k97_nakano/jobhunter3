package com.example.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.entity.ApplicationForm;
import com.example.repository.AccountRepository;
import com.example.service.ApplicationFormService;

@Controller
public class P4Controller {
	
	@Autowired
	ApplicationFormService applicationFormService;
	
	@Autowired
	AccountRepository accountRepository;

	@GetMapping("/p4fail")
	String p4fail() {
		return "p4/p4fail";
	}

	@RequestMapping(value = "/p4main", method = RequestMethod.GET)
	public String p4main(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		String userName = auth.getName();
		String judgmenttea = accountRepository.judgmentteacher(userName);
		if (judgmenttea.equals("0")) {
			return "redirect:/menu1";
		}else {
		List<ApplicationForm> Applist = applicationFormService.findAllstatus();
		model.addAttribute("Applist", Applist);
		return "p4/p4main";
		}
	}
	
	@GetMapping("/p4/{id}/p4success")//内容確認
	  public String edit(@PathVariable String id,Model model) { 
		  ApplicationForm Appform = applicationFormService.getOne(id); 
		  model.addAttribute("Appform",Appform);
	  return "p4/p4success"; 
	  }

	@RequestMapping(value = "/p4/{id}", params = "status=permission" ,method = RequestMethod.POST)//更新
	public String updatepermission(@PathVariable String id,@ModelAttribute("komento") String komento) {
		//appForm.setAppstatus("1");
		//appForm.setPermissionrefusal("申請済み");
		applicationFormService.saveKomento(komento,id);
		return "redirect:/p4main";
	}
	
	@RequestMapping(value = "/p4/{id}", params = "status=refusal" ,method = RequestMethod.POST)//更新
	public String updaterefusal(@PathVariable String id,@ModelAttribute("komento") String komento) {
		//appForm.setAppstatus("0");
		//appForm.setPermissionrefusal("申請拒否");
		applicationFormService.saveKomento(komento,id);
		return "redirect:/p4main";
	}
	//margetesttest

}
