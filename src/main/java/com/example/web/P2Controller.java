package com.example.web;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.domain.DbAccountDetails;
import com.example.dto.UserRequestReportForm;
import com.example.entity.ReportForm;
import com.example.repository.AccountRepository;
import com.example.service.ReportFormService;

@Controller
public class P2Controller {
	
	@Autowired
	ReportFormService reportFormService;
	
	@Autowired
	AccountRepository accountRepository;
	
	@GetMapping("/p2create")
	String p2create() {
		return "p2/p2create";
	}

	@GetMapping("/p2edit")
	String p2edit() {
		return "p2/p2edit";
	}

	@RequestMapping(value = "/p2create", method = RequestMethod.POST)
	public String create(@ModelAttribute UserRequestReportForm userRequestReportForm,
			@AuthenticationPrincipal DbAccountDetails dbAccountDetails) {
		reportFormService.create(userRequestReportForm, dbAccountDetails.getAccount());
		return "redirect:/p2main";
	}

	@RequestMapping(value = "/p2main", method = RequestMethod.GET)
	public String p2main(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		String userName = auth.getName();
		String judgmentstu = accountRepository.judgmentstudent(userName);
		if (judgmentstu.equals("0")) {
			return "redirect:/menu1";
		}else {
		List<ReportForm> Replist = reportFormService.findAlluserid(userName);
		model.addAttribute("Replist", Replist);
		return "p2/p2main";
		}
	}
	
	@GetMapping("/p2/{id}/p2edit")//編集 
	  public String edit(@PathVariable Integer id,Model model) { 
		  ReportForm Repform = reportFormService.getOne(id); 
		  model.addAttribute("Repform",Repform);
	  return "p2/p2edit"; 
	  }
	
	@PostMapping("/p2/{id}")//更新
	public String update(@PathVariable Integer id,@ModelAttribute ReportForm repForm,@AuthenticationPrincipal DbAccountDetails dbAccountDetails) {
		repForm.setId(id);
		LocalDateTime d = LocalDateTime.now();
		DateTimeFormatter df1 = 
				DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		String s = df1.format(d);
		repForm.setCurrent(s);
		repForm.setRepstatus("0");
		repForm.setPermissionrefusal("申請中");
		reportFormService.save(repForm,dbAccountDetails.getAccount());
		return "redirect:/p2main";
	}

	@PostMapping("/p2/delete") // 削除
	public String delete(Integer id) {
		reportFormService.delete(id);
		return "redirect:/p2main";
	}

}
