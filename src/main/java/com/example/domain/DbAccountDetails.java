package com.example.domain;

import org.springframework.security.core.authority.AuthorityUtils;

import com.example.entity.Account;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
public class DbAccountDetails extends org.springframework.security.core.userdetails.User {
	private static final long serialVersionUID = 1L;
	private Account account;
	
    public Account getAccount() {
        return account;
    }
    
	public void setAccount(Account account) {
		this.account = account;
	}

	public DbAccountDetails(Account account){
		super(account.getUserid(), account.getPassword(),
			AuthorityUtils.createAuthorityList("ROLE_USER"));
	        this.account = account;
	    }

}
