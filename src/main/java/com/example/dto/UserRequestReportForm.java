package com.example.dto;

public class UserRequestReportForm {
	
	private String current;
	private String checks;
	private String kigyoumei;
	private String sanmoji;
	private String hiniti;
	private String starttime;
	private String endtime;
	private String jyukenbasyo;
	private String naiyou1;
	private String naiyou2;
	private String naiyou3;
	private String kigyoumei1;
	private String kigyoumei2;
	private String kigyoumei3;
	private String kekka;
	private String ninzu;
	private String jikan;
	private String syutudainaiyou;
	private String komento;
	private String repstatus;
	public String getCurrent() {
		return current;
	}
	public void setCurrent(String current) {
		this.current = current;
	}
	public String getChecks() {
		return checks;
	}
	public void setChecks(String checks) {
		this.checks = checks;
	}
	public String getKigyoumei() {
		return kigyoumei;
	}
	public void setKigyoumei(String kigyoumei) {
		this.kigyoumei = kigyoumei;
	}
	public String getSanmoji() {
		return sanmoji;
	}
	public void setSanmoji(String sanmoji) {
		this.sanmoji = sanmoji;
	}
	public String getHiniti() {
		return hiniti;
	}
	public void setHiniti(String hiniti) {
		this.hiniti = hiniti;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getJyukenbasyo() {
		return jyukenbasyo;
	}
	public void setJyukenbasyo(String jyukenbasyo) {
		this.jyukenbasyo = jyukenbasyo;
	}
	public String getNaiyou1() {
		return naiyou1;
	}
	public void setNaiyou1(String naiyou1) {
		this.naiyou1 = naiyou1;
	}
	public String getNaiyou2() {
		return naiyou2;
	}
	public void setNaiyou2(String naiyou2) {
		this.naiyou2 = naiyou2;
	}
	public String getNaiyou3() {
		return naiyou3;
	}
	public void setNaiyou3(String naiyou3) {
		this.naiyou3 = naiyou3;
	}
	public String getKigyoumei1() {
		return kigyoumei1;
	}
	public void setKigyoumei1(String kigyoumei1) {
		this.kigyoumei1 = kigyoumei1;
	}
	public String getKigyoumei2() {
		return kigyoumei2;
	}
	public void setKigyoumei2(String kigyoumei2) {
		this.kigyoumei2 = kigyoumei2;
	}
	public String getKigyoumei3() {
		return kigyoumei3;
	}
	public void setKigyoumei3(String kigyoumei3) {
		this.kigyoumei3 = kigyoumei3;
	}
	public String getKekka() {
		return kekka;
	}
	public void setKekka(String kekka) {
		this.kekka = kekka;
	}
	public String getNinzu() {
		return ninzu;
	}
	public void setNinzu(String ninzu) {
		this.ninzu = ninzu;
	}
	public String getJikan() {
		return jikan;
	}
	public void setJikan(String jikan) {
		this.jikan = jikan;
	}
	public String getSyutudainaiyou() {
		return syutudainaiyou;
	}
	public void setSyutudainaiyou(String syutudainaiyou) {
		this.syutudainaiyou = syutudainaiyou;
	}
	public String getKomento() {
		return komento;
	}
	public void setKomento(String komento) {
		this.komento = komento;
	}
	public String getRepstatus() {
		return repstatus;
	}
	public void setRepstatus(String repstatus) {
		this.repstatus = repstatus;
	}
	

}
