package com.example.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "reportform")
public class ReportForm {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;
	private String current;
	private String checks;
	private String kigyoumei;
	private String sanmoji;
	private String hiniti;
	private String starttime;
	private String endtime;
	private String jyukenbasyo;
	private String naiyou1;
	private String naiyou2;
	private String naiyou3;
	private String kekka;
	private String ninzu;
	private String jikan;
	private String syutudainaiyou;
	private String komento;
	private String repstatus;
	private String permissionrefusal;
	@ManyToOne
	@JoinColumn(name="userid")
	private Account account;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCurrent() {
		return current;
	}

	public void setCurrent(String current) {
		this.current = current;
	}

	public String getChecks() {
		return checks;
	}

	public void setChecks(String checks) {
		this.checks = checks;
	}

	public String getKigyoumei() {
		return kigyoumei;
	}

	public void setKigyoumei(String kigyoumei) {
		this.kigyoumei = kigyoumei;
	}

	public String getSanmoji() {
		return sanmoji;
	}

	public void setSanmoji(String sanmoji) {
		this.sanmoji = sanmoji;
	}

	public String getHiniti() {
		return hiniti;
	}

	public void setHiniti(String hiniti) {
		this.hiniti = hiniti;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	public String getJyukenbasyo() {
		return jyukenbasyo;
	}

	public void setJyukenbasyo(String jyukenbasyo) {
		this.jyukenbasyo = jyukenbasyo;
	}

	public String getNaiyou1() {
		return naiyou1;
	}

	public void setNaiyou1(String naiyou1) {
		this.naiyou1 = naiyou1;
	}

	public String getNaiyou2() {
		return naiyou2;
	}

	public void setNaiyou2(String naiyou2) {
		this.naiyou2 = naiyou2;
	}

	public String getNaiyou3() {
		return naiyou3;
	}

	public void setNaiyou3(String naiyou3) {
		this.naiyou3 = naiyou3;
	}

	public String getKekka() {
		return kekka;
	}

	public void setKekka(String kekka) {
		this.kekka = kekka;
	}

	public String getNinzu() {
		return ninzu;
	}

	public void setNinzu(String ninzu) {
		this.ninzu = ninzu;
	}

	public String getJikan() {
		return jikan;
	}

	public void setJikan(String jikan) {
		this.jikan = jikan;
	}

	public String getSyutudainaiyou() {
		return syutudainaiyou;
	}

	public void setSyutudainaiyou(String syutudainaiyou) {
		this.syutudainaiyou = syutudainaiyou;
	}

	public String getKomento() {
		return komento;
	}

	public void setKomento(String komento) {
		this.komento = komento;
	}

	public String getRepstatus() {
		return repstatus;
	}

	public void setRepstatus(String repstatus) {
		this.repstatus = repstatus;
	}

	public String getPermissionrefusal() {
		return permissionrefusal;
	}

	public void setPermissionrefusal(String permissionrefusal) {
		this.permissionrefusal = permissionrefusal;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	/*
	 * create table reportform(id int(100) AUTO_INCREMENT PRIMARY
	 * KEY,current varchar(100),checks varchar(100),kigyoumei varchar(100),sanmoji 
	 * varchar(100),hiniti varchar(100),starttime varchar(100),
	 * endtime varchar(100),jyukenbasyo varchar(100),
	 * naiyou1 varchar(100),naiyou2 varchar(100),naiyou3 varchar(100),
	 * kekka varchar(100),ninzu varchar(100),jikan varchar(100),
	 * syutudainaiyou varchar(100),komento varchar(100),repstatus varchar(10),
	 * permissionrefusal varchar(100),userid varchar(255),FOREIGN KEY (userid) REFERENCES accounts(userid));
	 */
	
	
	

}
