package com.example.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "teachercord")
public class TeacherCord {
	@Id
	private String teachercord;

	public String getTeachercord() {
		return teachercord;
	}

	public void setTeachercord(String teachercord) {
		this.teachercord = teachercord;
	}
	
	//create table teachercord(teachercord varchar(255));

}
